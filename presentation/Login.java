package ro.tuc.pt.asig3.presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import ro.tuc.pt.asig3.dao.ClientDAO;
import ro.tuc.pt.asig3.model.Client;

public class Login extends JFrame{

	private static final long serialVersionUID = 1L;
	private static JButton btnLogin;
    private static JButton btnRegister;
    private static JTextField txtID;
    private static JPasswordField txtPass;
    private JLabel lblID;
    private JLabel lblPass;

    public Login(ClientDAO clientDAO) {
        //construct components
        btnLogin = new JButton("LogIn");
        btnRegister = new JButton("Register");
        txtID = new JTextField();
        txtPass = new JPasswordField();
        lblID = new JLabel("User ID:");
        lblPass = new JLabel("Password:");

        //adjust size and set layout
        this.setSize(366, 250);
        this.setLayout (null);

        //add components
        this.add(btnLogin);
        this.add(btnRegister);
        this.add(txtID);
        this.add(txtPass);
        this.add(lblID);
        this.add(lblPass);

        //set component bounds (only needed by Absolute Positioning)
        btnLogin.setBounds (115, 150, 100, 20);
        btnRegister.setBounds (225, 150, 100, 20);
        txtID.setBounds (80, 45, 250, 25);
        txtPass.setBounds (80, 90, 250, 25);
        lblID.setBounds (10, 45, 100, 25);
        lblPass.setBounds (10, 90, 100, 25);
        
        this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        this.setVisible (true);
        this.setTitle("Login");
        btnLogin.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String id = txtID.getText();
                Client client = clientDAO.findByEmail(id, true);
                String pass = "";
                char[] passw = txtPass.getPassword();
                if(client!=null){
                	pass = client.getPass();
                }
                
                if(Arrays.equals(pass.toCharArray(), passw)){
                	System.out.println("Success");
                	if(client.getPriv() != 0)
                	{
                		Login.this.setVisible(false);
                		Admin admin = new Admin();
                	}
                	else{
                		Login.this.setVisible(false);
                		ClientView cv = new ClientView(clientDAO, client.getID());
                	}
                }
                else{
                	System.out.println("Wrong Pass");
                	infoBox("Wrong Login Information", "Error");
                }
			}
		});
        
        btnRegister.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnRegisterActionPerformed(evt, clientDAO);
            }
        });
    }
    
//    public static void btnLoginActionListener (ClientDAO clientDAO){
//		Login.btnLogin.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				String id = txtID.getText();
//                Client client = clientDAO.read(Integer.parseInt(id), true);
//                String pass = "";
//                char[] passw = txtPass.getPassword();
//                if(client!=null){
//                	pass = client.getPass();
//                }
//                
//                if(Arrays.equals(pass.toCharArray(), passw)){
//                	System.out.println("Success");
//                	Login.this.setVisible(false);
//                }
//                else{
//                	System.out.println("Wrong Pass");
//                	infoBox("Wrong Login Information", "Error");
//                }
//			}
//		});
//	}
    
    private void btnRegisterActionPerformed(ActionEvent evt, ClientDAO clientDAO) {                                         
    	Register reg = new Register(clientDAO);
    }
    
    public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage,titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
}
