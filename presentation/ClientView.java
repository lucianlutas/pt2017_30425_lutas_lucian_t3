package ro.tuc.pt.asig3.presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.table.TableModel;

import ro.tuc.pt.asig3.bll.ClientBLL;
import ro.tuc.pt.asig3.bll.ProductBLL;
import ro.tuc.pt.asig3.dao.ClientDAO;

public class ClientView extends JFrame{

	private static final long serialVersionUID = 1L;
	private JButton btnAcc;
    private JButton btnCart;
    private JButton btnOrder;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JScrollPane jScrollPane1;
    public static JTable tblProd;
    private JTextField txtProd;
    private JTextField txtQuantity;
    private int prodID;
    private int accID;
	
	public ClientView(ClientDAO clientDAO, int id){
		
		this.accID = id;
		
		btnAcc = new JButton();
        jScrollPane1 = new JScrollPane();
        txtProd = new JTextField();
        jLabel1 = new JLabel();
        btnOrder = new JButton();
        btnCart = new JButton();
        jLabel2 = new JLabel();
        txtQuantity = new JTextField();
        tblProd = new JTable(){
        	private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {                
                    return false;               
            };
        };
        
        prodID = 0;

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        btnAcc.setText("Edit Account");
        btnAcc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAccActionPerformed(evt, clientDAO, id);
            }
        });
        
        btnOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnOrderActionPerformed(evt, id);
            }
        });
        
        tblProd.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
            	tblProdMouseClicked(evt);
            }
        });
        
        btnCart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCartActionPerformed(evt);
            }
        });

        tblProd.setModel(ProductBLL.populate());

        jScrollPane1.setViewportView(tblProd);
        
        tblProd.setEnabled(true);
        
        txtProd.setEditable(false);

        jLabel1.setText("Product");

        btnOrder.setText("Add To Cart");

        btnCart.setText("Cart");

        jLabel2.setText("Quantity");

        txtQuantity.setText("1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAcc)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCart)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(txtProd, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnOrder, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAcc)
                    .addComponent(btnCart))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19)
                        .addComponent(btnOrder)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnAcc.getAccessibleContext().setAccessibleName("btnAcc");

        pack();
        this.setTitle("Shop");
        this.setVisible(true);
	}
	
	private void tblProdMouseClicked(MouseEvent evt) {                                     
        int i = tblProd.getSelectedRow();
        TableModel model = tblProd.getModel();
        prodID = Integer.parseInt(model.getValueAt(i,0).toString());
        txtProd.setText(model.getValueAt(i,1).toString());
        txtQuantity.setText(model.getValueAt(i,2).toString());
    } 
	
	private void btnAccActionPerformed(ActionEvent evt, ClientDAO clientDAO, int id) {    
		
		if(ClientBLL.changeAcc(clientDAO, id)){
			Login.infoBox("Account updated!", "Info");
		}
		else{
			Login.infoBox("Account not updated!", "Info");
		}
	}     
	
	private void btnOrderActionPerformed(ActionEvent evt, int id) {                                         
        if(ProductBLL.addToCart(Integer.parseInt(txtQuantity.getText()), prodID, id, txtProd.getText())==1){
        	tblProd.setModel(ProductBLL.populate());
        	Login.infoBox("Product added to cart", "Info");
        }
        else if(ProductBLL.addToCart(Integer.parseInt(txtQuantity.getText()), prodID, id, txtProd.getText()) == -1){
        	Login.infoBox("Not enough in stock", "Info");
        }
        else{
        	Login.infoBox("Product not added to cart", "Info");
        }
    }    
	
	private void btnCartActionPerformed(ActionEvent evt) {                                         
        CartView cv = new CartView(accID);
    }    
	
}
