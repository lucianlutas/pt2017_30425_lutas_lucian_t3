package ro.tuc.pt.asig3.presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import ro.tuc.pt.asig3.bll.CartBLL;
import ro.tuc.pt.asig3.bll.ProductBLL;

public class CartView extends JFrame{
	private static final long serialVersionUID = 1L;
	private JButton btnOrder;
    private JButton btnRemove;
    private JLabel jLabel1;
    private JScrollPane jScrollPane1;
    public static JTable tblCart;
    private JTextField txtProd;
    private JButton btnOrderAll;
    private int cartID;
    private int itemNo;
    
    public CartView(int cartID){
    	
    	this.cartID = cartID;
    	
    	jScrollPane1 = new javax.swing.JScrollPane();
        txtProd = new JTextField();
        jLabel1 = new JLabel();
        btnOrder = new JButton();
        btnOrderAll = new JButton();
        btnRemove = new JButton();
        tblCart = new JTable(){
        	private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {                
                    return false;               
            };
        };

        tblCart.setModel(CartBLL.populate(false, cartID));
        
        itemNo = 0;
        
        tblCart.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblCart);

        txtProd.setEditable(false);

        jLabel1.setText("Product");

        btnOrder.setText("Order");
        btnOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnOrderActionPerformed(evt);
            }
        });
        
        tblCart.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
            	tblProdMouseClicked(evt);
            }
        });

        btnRemove.setText("Remove");
        btnRemove.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	btnRemoveActionPerformed(evt);
            }
        });
        
        btnOrderAll.setText("Order All");
        btnOrderAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnOrderAllActionPerformed(evt);
            }
        });
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addGap(18, 18, 18)
                            .addComponent(txtProd, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(btnRemove)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnOrder)))
                    .addComponent(btnOrderAll, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnOrder)
                            .addComponent(btnRemove))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnOrderAll))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        this.setVisible(true);
        this.setTitle("Cart");
    }
    
    private void tblProdMouseClicked(MouseEvent evt) {                                     
        int i = tblCart.getSelectedRow();
        TableModel model = tblCart.getModel();
        itemNo = Integer.parseInt(model.getValueAt(i,0).toString());
        txtProd.setText(model.getValueAt(i,1).toString());
    } 
    
    private void btnOrderActionPerformed(ActionEvent evt) {        
    	
    	if(CartBLL.makeOrder(itemNo, txtProd.getText())){
    		tblCart.setModel(CartBLL.populate(false, cartID));
    		Login.infoBox("Product Ordered", "Info");
    	}
    	else
    	{
    		Login.infoBox("Product was not ordered", "Info");
    	}
    	txtProd.setText("");
    }   
    
    private void btnOrderAllActionPerformed(ActionEvent evt) {       	
    	CartBLL.makeOrderAll(tblCart.getModel());
    	tblCart.setModel(CartBLL.populate(false, cartID));
	    Login.infoBox("Products Ordered ", "Info");
	    txtProd.setText("");
    }    
    
    private void btnRemoveActionPerformed(ActionEvent evt) {                                         
    	if(CartBLL.removeOrder(itemNo, txtProd.getText())){
    		tblCart.setModel(CartBLL.populate(false, cartID));
        	ClientView.tblProd.setModel(ProductBLL.populate());
        	Login.infoBox("Product Removed ", "Info");
    	}
    	else
    	{
    		Login.infoBox("No product selected", "Info");
    	}
        txtProd.setText("");
        
    } 
}
