package ro.tuc.pt.asig3.presentation;

import javax.swing.JOptionPane;

import ro.tuc.pt.asig3.dao.ClientDAO;
import ro.tuc.pt.asig3.model.Client;

public class Register{

    public Register(ClientDAO clientDAO) {
    	String name = JOptionPane.showInputDialog(null, "Name: ", "");
    	String surname = JOptionPane.showInputDialog(null, "Surname: ", "");
    	String passw = JOptionPane.showInputDialog(null, "Password: ", "");
    	String email = JOptionPane.showInputDialog(null, "Email: ", "");
    	if((name != null) && (name.length() > 0) && (surname != null) && (surname.length() > 0) && (passw != null) && (passw.length() > 0)){
	    	Client client = new Client();
	    	client.setName(name);
	    	client.setSurname(surname);
	    	client.setPass(passw);
	    	client.setEmail(email);
	    	int id = clientDAO.create(client);
	    	Login.infoBox("Account with ID " + id + " was created!", "Info");
    	}
    	else{
    		Login.infoBox("Account not created!", "Info");
    	}
    }
}
