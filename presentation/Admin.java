package ro.tuc.pt.asig3.presentation;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.table.TableModel;

import ro.tuc.pt.asig3.bll.CartBLL;
import ro.tuc.pt.asig3.bll.ClientBLL;
import ro.tuc.pt.asig3.bll.OrderBLL;
import ro.tuc.pt.asig3.bll.ProductBLL;

public class Admin extends JFrame{

	private static final long serialVersionUID = 1L;
	private JButton btnAddQuant;
    private JButton btnCreate;
    private JButton btnCreateCart;
    private JButton btnCreateOrder;
    private JButton btnCreateProd;
    private JButton btnDelete;
    private JButton btnDeleteCart;
    private JButton btnDeleteOrder;
    private JButton btnDeleteProd;
    private JButton btnSubQuant;
    private JButton btnUpdate;
    private JButton btnUpdateCart;
    private JButton btnUpdateOrder;
    private JButton btnUpdateProd;
    private JPanel jPanel1;
    private JPanel jPanel3;
    private JPanel jPanel6;
    private JPanel jPanel7;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane5;
    private JScrollPane jScrollPane6;
    private JScrollPane jScrollPane7;
    private JTabbedPane jTabbedPane1;
    private JLabel lblCartID;
    private JLabel lblCartProdName;
    private JLabel lblCartProdQuant;
    private JLabel lblClientID;
    private JLabel lblClientID4;
    private JLabel lblClientID6;
    private JLabel lblEmail;
    private JLabel lblName;
    private JLabel lblOrderCartID;
    private JLabel lblOrderID;
    private JLabel lblPass;
    private JLabel lblPriv;
    private JLabel lblProdName;
    private JLabel lblProdQuant;
    private JLabel lblSurname;
    private JLabel lblUpdateQuant;
    private JTable tblCarts;
    private JTable tblClients;
    private JTable tblOrders;
    private JTable tblProducts;
    private JTextField txtCartID;
    private JTextField txtCartProdName;
    private JTextField txtCartProdQuant;
    private JTextField txtClientID;
    private JTextField txtEmail;
    private JTextField txtItemNo;
    private JTextField txtName;
    private JTextField txtOrderCartID;
    private JTextField txtOrderID;
    private JTextField txtPass;
    private JTextField txtPriv;
    private JTextField txtProdID;
    private JTextField txtProdName;
    private JTextField txtProdQuant;
    private JTextField txtSurname;
    private JTextField txtUpdateQuant;
	
	public Admin(){
		jTabbedPane1 = new JTabbedPane();
        jPanel1 = new JPanel();
        jScrollPane1 = new JScrollPane();
        tblClients = new JTable(){
        	private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {                
                    return false;               
            };
        };
        lblClientID = new JLabel();
        txtClientID = new JTextField();
        lblName = new JLabel();
        txtName = new JTextField();
        txtSurname = new JTextField();
        lblSurname = new JLabel();
        lblPass = new JLabel();
        txtPass = new JTextField();
        lblPriv = new JLabel();
        txtPriv = new JTextField();
        txtEmail = new JTextField();
        lblEmail = new JLabel();
        btnCreate = new JButton();
        btnDelete = new JButton();
        btnUpdate = new JButton();
        jPanel3 = new JPanel();
        jScrollPane5 = new JScrollPane();
        tblProducts = new JTable(){
        	private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {                
                    return false;               
            };
        };
        lblClientID4 = new JLabel();
        txtProdID = new JTextField();
        lblProdName = new JLabel();
        txtProdName = new JTextField();
        txtProdQuant = new JTextField();
        lblProdQuant = new JLabel();
        txtUpdateQuant = new JTextField();
        lblUpdateQuant = new JLabel();
        btnCreateProd = new JButton();
        btnDeleteProd = new JButton();
        btnUpdateProd = new JButton();
        btnAddQuant = new JButton();
        btnSubQuant = new JButton();
        jPanel6 = new JPanel();
        jScrollPane6 = new JScrollPane();
        tblOrders = new JTable(){
        	private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {                
                    return false;               
            };
        };
        lblOrderID = new JLabel();
        txtOrderID = new JTextField();
        lblOrderCartID = new JLabel();
        txtOrderCartID = new JTextField();
        btnCreateOrder = new JButton();
        btnDeleteOrder = new JButton();
        btnUpdateOrder = new JButton();
        jPanel7 = new JPanel();
        jScrollPane7 = new JScrollPane();
        tblCarts = new JTable(){
        	private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {                
                    return false;               
            };
        };
        lblClientID6 = new JLabel();
        txtItemNo = new JTextField();
        lblCartID = new JLabel();
        txtCartID = new JTextField();
        txtCartProdName = new JTextField();
        lblCartProdName = new JLabel();
        lblCartProdQuant = new JLabel();
        txtCartProdQuant = new JTextField();
        btnCreateCart = new JButton();
        btnDeleteCart = new JButton();
        btnUpdateCart = new JButton();

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        jTabbedPane1.setName("Clients"); // NOI18N
        
        tblClients.setModel(ClientBLL.populate());
        tblProducts.setModel(ProductBLL.populate());
        tblCarts.setModel(CartBLL.populate(true, 1));
        tblOrders.setModel(OrderBLL.populate());
        
        
        jScrollPane1.setViewportView(tblClients);

        lblClientID.setText("Client ID");

        lblName.setText("Name");

        lblSurname.setText("Surname");

        lblPass.setText("Password");

        lblPriv.setText("Priviledge");

        lblEmail.setText("Email");

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        
        tblClients.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
            	tblClientsMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 595, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblClientID)
                            .addComponent(lblName))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtName)
                            .addComponent(txtClientID)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPass)
                            .addComponent(lblSurname))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSurname)
                            .addComponent(txtPass, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPriv)
                            .addComponent(lblEmail))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtEmail)
                            .addComponent(txtPriv, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCreate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUpdate)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblClientID, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtClientID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSurname, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPass, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPriv, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPriv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreate)
                    .addComponent(btnDelete)
                    .addComponent(btnUpdate))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Clients", jPanel1);

        jScrollPane5.setViewportView(tblProducts);

        lblClientID4.setText("Product ID");

        lblProdName.setText("Name");

        lblProdQuant.setText("Quantity");

        lblUpdateQuant.setText("Update Quantity");

        btnCreateProd.setText("Create");
        btnCreateProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateProdActionPerformed(evt);
            }
        });

        btnDeleteProd.setText("Delete");
        btnDeleteProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteProdActionPerformed(evt);
            }
        });

        btnUpdateProd.setText("Update");
        btnUpdateProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateProdActionPerformed(evt);
            }
        });

        btnAddQuant.setText("Add");
        btnAddQuant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddQuantActionPerformed(evt);
            }
        });

        btnSubQuant.setText("Subtract");
        btnSubQuant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubQuantActionPerformed(evt);
            }
        });
        
        tblProducts.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
            	tblProductsMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 595, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblClientID4)
                            .addComponent(lblProdName))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtProdName)
                            .addComponent(txtProdID)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblProdQuant)
                        .addGap(24, 24, 24)
                        .addComponent(txtProdQuant))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblUpdateQuant)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtUpdateQuant))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 71, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addComponent(btnCreateProd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDeleteProd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnUpdateProd))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addComponent(btnAddQuant)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSubQuant)))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblClientID4, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProdID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblProdName, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblProdQuant, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProdQuant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreateProd)
                    .addComponent(btnDeleteProd)
                    .addComponent(btnUpdateProd))
                .addGap(27, 27, 27)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUpdateQuant, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUpdateQuant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddQuant)
                    .addComponent(btnSubQuant))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Products", jPanel3);

        jScrollPane6.setViewportView(tblOrders);

        lblOrderID.setText("Order ID");

        lblOrderCartID.setText("Cart ID");

        btnCreateOrder.setText("Create");
        btnCreateOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateOrderActionPerformed(evt);
            }
        });

        btnDeleteOrder.setText("Delete");
        btnDeleteOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteOrderActionPerformed(evt);
            }
        });

        btnUpdateOrder.setText("Update");
        btnUpdateOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateOrderActionPerformed(evt);
            }
        });
        
        tblOrders.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
            	tblOrdersMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 595, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblOrderID)
                            .addComponent(lblOrderCartID))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtOrderCartID)
                            .addComponent(txtOrderID)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGap(0, 71, Short.MAX_VALUE)
                        .addComponent(btnCreateOrder)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDeleteOrder)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUpdateOrder)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOrderID, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtOrderID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOrderCartID, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtOrderCartID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreateOrder)
                    .addComponent(btnDeleteOrder)
                    .addComponent(btnUpdateOrder))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Orders", jPanel6);

        jScrollPane7.setViewportView(tblCarts);

        lblClientID6.setText("ItemNo");

        lblCartID.setText("Cart ID");

        lblCartProdName.setText("Product Name");

        lblCartProdQuant.setText("Quantity");

        btnCreateCart.setText("Create");
        btnCreateCart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateCartActionPerformed(evt);
            }
        });

        btnDeleteCart.setText("Delete");
        btnDeleteCart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteCartActionPerformed(evt);
            }
        });

        btnUpdateCart.setText("Update");
        btnUpdateCart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateCartActionPerformed(evt);
            }
        });

        tblCarts.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
            	tblCartsMouseClicked(evt);
            }
        });
        
        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 595, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblClientID6)
                            .addComponent(lblCartID))
                        .addGap(44, 44, 44)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCartID)
                            .addComponent(txtItemNo)))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCartProdQuant)
                            .addComponent(lblCartProdName))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCartProdName)
                            .addComponent(txtCartProdQuant, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCreateCart)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDeleteCart)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUpdateCart)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblClientID6, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtItemNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCartID, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCartID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCartProdName, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCartProdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCartProdQuant, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCartProdQuant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreateCart)
                    .addComponent(btnDeleteCart)
                    .addComponent(btnUpdateCart))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Carts", jPanel7);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("Clients");

        pack();
        
        this.setVisible(true);
        this.setTitle("Admin Panel");
    }
	
	private void tblClientsMouseClicked(MouseEvent evt) {                                     
        int i = tblClients.getSelectedRow();
        TableModel model = tblClients.getModel();
        txtClientID.setText(model.getValueAt(i,0).toString());
        txtName.setText(model.getValueAt(i,1).toString());
        txtSurname.setText(model.getValueAt(i,2).toString());
        txtPass.setText(model.getValueAt(i,3).toString());
        txtPriv.setText(model.getValueAt(i,4).toString());
        txtEmail.setText(model.getValueAt(i,5).toString());
    }

	private void tblProductsMouseClicked(MouseEvent evt) {                                     
        int i = tblProducts.getSelectedRow();
        TableModel model = tblProducts.getModel();
        txtProdID.setText(model.getValueAt(i,0).toString());
        txtProdName.setText(model.getValueAt(i,1).toString());
        txtProdQuant.setText(model.getValueAt(i,2).toString());
    }
	
	private void tblOrdersMouseClicked(MouseEvent evt) {                                     
        int i = tblOrders.getSelectedRow();
        TableModel model = tblOrders.getModel();
        txtOrderID.setText(model.getValueAt(i,0).toString());
        txtOrderCartID.setText(model.getValueAt(i,1).toString());
    }
	
	private void tblCartsMouseClicked(MouseEvent evt) {                                     
        int i = tblCarts.getSelectedRow();
        TableModel model = tblCarts.getModel();
        txtItemNo.setText(model.getValueAt(i,0).toString());
        txtCartID.setText(model.getValueAt(i,1).toString());
        txtCartProdName.setText(model.getValueAt(i,2).toString());
        txtCartProdQuant.setText(model.getValueAt(i,3).toString());
    }
	
	protected void btnUpdateCartActionPerformed(ActionEvent evt) {
		if(CartBLL.adminChangeCart(false, txtItemNo.getText(), txtCartID.getText(), txtCartProdName.getText(), txtCartProdQuant.getText())){
			tblCarts.setModel(CartBLL.populate(true, 1));
        	Login.infoBox("Product updated", "Info");
		}
		else{
			Login.infoBox("Product not updated", "Info");
		}
		
	}

	protected void btnDeleteCartActionPerformed(ActionEvent evt) {
		if(CartBLL.deleteProd(txtItemNo.getText())){
			tblCarts.setModel(CartBLL.populate(true, 1));
			txtItemNo.setText("");
			txtCartID.setText("");
			txtCartProdName.setText("");
			txtCartProdQuant.setText("");
        	Login.infoBox("Product deleted", "Info");
		}
		else{
			Login.infoBox("Product not deleted", "Info");
		}
		
	}

	protected void btnCreateCartActionPerformed(ActionEvent evt) {
		if(CartBLL.adminChangeCart(true, txtItemNo.getText(), txtCartID.getText(), txtCartProdName.getText(), txtCartProdQuant.getText())){
			tblCarts.setModel(CartBLL.populate(true, 1));
        	Login.infoBox("Product created", "Info");
		}
		else{
			Login.infoBox("Product not created", "Info");
		}
		
	}

	protected void btnUpdateOrderActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		
	}

	protected void btnDeleteOrderActionPerformed(ActionEvent evt) {
		
	}

	protected void btnCreateOrderActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		
	}

	protected void btnSubQuantActionPerformed(ActionEvent evt) {
		if(ProductBLL.changeProductQuant(false, txtProdID.getText(), txtProdName.getText(), Integer.parseInt(txtUpdateQuant.getText()))){
			tblProducts.setModel(ProductBLL.populate());
        	Login.infoBox("Product updated", "Info");
		}
		else{
			Login.infoBox("Product not updated", "Info");
		}
	}

	protected void btnAddQuantActionPerformed(ActionEvent evt) {
		if(ProductBLL.changeProductQuant(true, txtProdID.getText(), txtProdName.getText(), Integer.parseInt(txtUpdateQuant.getText()))){
			tblProducts.setModel(ProductBLL.populate());
        	Login.infoBox("Product updated", "Info");
		}
		else{
			Login.infoBox("Product not updated", "Info");
		}
		
	}

	protected void btnUpdateProdActionPerformed(ActionEvent evt) {
		if(ProductBLL.adminChangeProduct(false, txtProdID.getText(), txtProdName.getText(), txtProdQuant.getText())){
			tblProducts.setModel(ProductBLL.populate());
        	Login.infoBox("Product updated", "Info");
		}
		else{
			Login.infoBox("Product not updated", "Info");
		}
		
	}

	protected void btnDeleteProdActionPerformed(ActionEvent evt) {
		if(ProductBLL.deleteProd(txtProdID.getText())){
			tblProducts.setModel(ProductBLL.populate());
			txtProdID.setText("");
			txtProdName.setText("");
			txtProdQuant.setText("");
        	Login.infoBox("Product deleted", "Info");
		}
		else{
			Login.infoBox("Product not deleted", "Info");
		}
		
	}

	protected void btnCreateProdActionPerformed(ActionEvent evt) {
		if(ProductBLL.adminChangeProduct(true, txtProdID.getText(), txtProdName.getText(), txtProdQuant.getText())){
			tblProducts.setModel(ProductBLL.populate());
        	Login.infoBox("Product created", "Info");
		}
		else{
			Login.infoBox("Product not created", "Info");
		}
		
	}

	protected void btnUpdateActionPerformed(ActionEvent evt) {
		if(ClientBLL.adminChangeAcc(false, txtClientID.getText(), txtName.getText(), txtSurname.getText(), txtPass.getText(), txtPriv.getText(), txtEmail.getText())){
			tblClients.setModel(ClientBLL.populate());
        	Login.infoBox("Client updated", "Info");
		}
		else{
			Login.infoBox("Client not updated", "Info");
		}
	}

	protected void btnDeleteActionPerformed(ActionEvent evt) {
		if(ClientBLL.deleteClient(txtClientID.getText())){
			tblClients.setModel(ClientBLL.populate());
			txtClientID.setText("");
			txtName.setText("");
			txtSurname.setText("");
			txtPass.setText("");
			txtPriv.setText("");
			txtEmail.setText("");
        	Login.infoBox("Client deleted", "Info");
		}
		else{
			Login.infoBox("Client not deleted", "Info");
		}
		
	}

	protected void btnCreateActionPerformed(ActionEvent evt) {
		if(ClientBLL.adminChangeAcc(true, txtClientID.getText(), txtName.getText(), txtSurname.getText(), txtPass.getText(), txtPriv.getText(), txtEmail.getText())){
			tblClients.setModel(ClientBLL.populate());
        	Login.infoBox("Client created", "Info");
		}
		else{
			Login.infoBox("Client not created", "Info");
		}
		
	}
	
}
