package ro.tuc.pt.asig3.start;

import ro.tuc.pt.asig3.dao.ClientDAO;
import ro.tuc.pt.asig3.presentation.ClientView;
import ro.tuc.pt.asig3.presentation.Login;

public class Start {
	
	static Login login;
	static ClientView cv;
	
	public static void main(String[] args) {
        ClientDAO clientDAO = new ClientDAO();
        login = new Login(clientDAO);         
    }
}
