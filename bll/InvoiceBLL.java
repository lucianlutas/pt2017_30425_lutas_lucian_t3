package ro.tuc.pt.asig3.bll;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import java.util.Properties;
import javax.activation.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class InvoiceBLL {
	
	public static String createInvoice(String str){
		Document document = new Document();
		SimpleDateFormat sdf = new SimpleDateFormat("-yyyy-MM-dd-HH-mm-ss");
		Date today = Calendar.getInstance().getTime();  
		String reportDate = sdf.format(today);
		String fileName = "invoice"+reportDate.toString()+".pdf";
		try{
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
			document.open();
			Paragraph paragraph = new Paragraph();
			paragraph.add("INVOICE: \n\nThe following products were ordered: \n"+str+"\n Thank you for your purchase!");
			document.add(paragraph);
			document.close();
			writer.close();
			return fileName;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void sendEmail(String emailAddress, String str){
		final String username = "lucilutas@gmail.com"; //ur email
	    final String password = "22mppp22mppp";

	    Properties props = new Properties();
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.starttls.enable", "true");
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.port", "587");
	    props.put("mail.smtp.starttls.required", "true");

	    Session session = Session.getInstance(props, new javax.mail.Authenticator() {
		    protected PasswordAuthentication getPasswordAuthentication() {
		        return new PasswordAuthentication(username, password);
		    }                            
	    });

	    try {
	        
	        Message message = new MimeMessage(session);
	        message.setFrom(new InternetAddress("lucilutas@gmail.com"));
	        message.setRecipients(Message.RecipientType.TO,
	                InternetAddress.parse(emailAddress));
	        message.setSubject("Invoice");    
	        message.setText("-");
	        MimeBodyPart messageBodyPart = new MimeBodyPart();
	        Multipart multipart = new MimeMultipart();

		    //attached 1 --------------------------------------------
		        String file = createInvoice(str);
		        String fileName = "Invoice.pdf";
		    messageBodyPart = new MimeBodyPart();   
		    DataSource source = new FileDataSource(file);      
		    messageBodyPart.setDataHandler(new DataHandler(source));
		    messageBodyPart.setFileName(fileName);
		    multipart.addBodyPart(messageBodyPart);
		    //------------------------------------------------------    

	        message.setContent(multipart);	    
	        System.out.println("sending");
	        Transport.send(message);
	        System.out.println("Done");
	            
		}catch (MessagingException e) {
		        e.printStackTrace();
		}
	}
	
}
