package ro.tuc.pt.asig3.bll;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.table.TableModel;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import net.proteanit.sql.DbUtils;
import ro.tuc.pt.asig3.connectivity.ConnectionFactory;
import ro.tuc.pt.asig3.dao.CartDAO;
import ro.tuc.pt.asig3.model.Cart;

public class CartBLL {
	
	public static TableModel populate(boolean admin, int id){
    	Connection conn = null;
    	ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = ConnectionFactory.getConnection();
            if(admin){
            	preparedStatement = (PreparedStatement) conn.prepareStatement("select * from cart");
            }
            else{
            	preparedStatement = (PreparedStatement) conn.prepareStatement("select itemNo, prodName, productQuantity from cart where cartID = ?");
            	preparedStatement.setInt(1, id);
            }
            rs = preparedStatement.executeQuery();
            return DbUtils.resultSetToTableModel(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
        return DbUtils.resultSetToTableModel(rs);
    }
	
	public static void addToCart(int id, int quant, String txtProd){	
		CartDAO cartDAO = new CartDAO();
		Cart cart = new Cart();
		cart.setID(id);
        cart.setProdName(txtProd);
        cart.setProdQuantity(quant);
        cartDAO.create(cart);   
	}
	
	public static boolean makeOrder(int itemNo, String txtProd){
		CartDAO cartDAO = new CartDAO();
        Cart cart = null;
        String str = "\n";
        if((txtProd != null) && (txtProd.length() > 0)){
        	cart = cartDAO.read(itemNo);
        	str+="\t-> Product "+cart.getProdName()+" , Quantity "+cart.getProdQuantity()+"\n";
        	cartDAO.delete(itemNo);
        	str += "\n Products will be delivered to "+ClientBLL.returnClient(cart.getID()).getName()+" "+ClientBLL.returnClient(cart.getID()).getSurname()+"\n";
            InvoiceBLL.sendEmail(ClientBLL.returnClient(cart.getID()).getEmail(), str);
        	return true;
        }else{
        	return false;
        }
	}
	
	public static void makeOrderAll(TableModel dtm){
		CartDAO cartDAO = new CartDAO();
		int id = 0;
		String str = "\n";
		Cart cart = null;
  		int nRow = dtm.getRowCount();
  		for(int i = 0; i<nRow; i++){
  			id = (int) dtm.getValueAt(i,0);
  			cart = cartDAO.read(id);
  			str+="\t-> Product "+cart.getProdName()+" , Quantity "+cart.getProdQuantity()+"\n";
  			cartDAO.delete(id);
  		}
  		str += "\n Products will be delivered to "+ClientBLL.returnClient(cart.getID()).getName()+" "+ClientBLL.returnClient(cart.getID()).getSurname()+"\n";
  		InvoiceBLL.sendEmail(ClientBLL.returnClient(cart.getID()).getEmail(), str);
	}
	
	public static boolean removeOrder(int itemNo, String txtProd){
		CartDAO cartDAO = new CartDAO();
        Cart cart = null;
        if((txtProd != null) && (txtProd.length() > 0)){
        	cart = cartDAO.read(itemNo);
        	int quan = cart.getProdQuantity();
        	ProductBLL.updateProductQuant(txtProd, quan);
        	cartDAO.delete(itemNo);
        	return true;
        }
        else{
        	return false;
        }
	}
	
	public static boolean adminChangeCart(boolean create, String itemNo, String id, String name, String quant){
		if((itemNo != null) && (itemNo.length() > 0) && (name != null) && (name.length() > 0) && (id != null) && (id.length() > 0) && (quant != null) && (quant.length() > 0)){
	    	Cart cart = new Cart();
	    	CartDAO cartDAO = new CartDAO();
	    	cart.setID(Integer.parseInt(id));
	    	cart.setItemNo(Integer.parseInt(itemNo));
	    	cart.setProdQuantity(Integer.parseInt(quant));
	    	cart.setProdName(name);
	    	if(create){
	    		if(cartDAO.createCheck(Integer.parseInt(itemNo))){
	    			return false;
	    		}
	    		cartDAO.create(cart);
	    	}
	    	else{
	    		cartDAO.update(cart);
	    	}
	    	return true;
    	}
    	else{
    		return false;
    	}
        
	}
	
	public static boolean deleteProd(String idToRemove){
		if((idToRemove != null) && (idToRemove.length() > 0)){
			int id = Integer.parseInt(idToRemove);
			CartDAO cartDAO = new CartDAO();
			cartDAO.delete(id);
			return true;
		}
		else{
			return false;
		}
	}
}
