package ro.tuc.pt.asig3.bll;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.table.TableModel;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import net.proteanit.sql.DbUtils;
import ro.tuc.pt.asig3.connectivity.ConnectionFactory;
import ro.tuc.pt.asig3.dao.ProductDAO;
import ro.tuc.pt.asig3.model.Product;

public class ProductBLL {

	public static TableModel populate(){
    	Connection conn = null;
    	ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = ConnectionFactory.getConnection();
            preparedStatement = (PreparedStatement) conn.prepareStatement("select * from product");
            rs = preparedStatement.executeQuery();
            return DbUtils.resultSetToTableModel(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
        return DbUtils.resultSetToTableModel(rs);
    }
	
	public static int addToCart(int quantity, int id1, int id2, String txtProd){
        ProductDAO productDAO = new ProductDAO();
        Product product = new Product();
        int quant = quantity;
        
        product = productDAO.read(id1);
        
        if((txtProd != null) && (txtProd.length() > 0)){
        	if((product.getQuantity() >= quant) && (quant > 0)){
		        CartBLL.addToCart(id2, quant, txtProd);
		        product.setQuantity(product.getQuantity() - quant);
		        productDAO.update(product);
		        return 1;
        	}
        	else{
        		return -1;
        	}
        }else{
        	return 0;
        }
        
	}
	
	public static boolean adminChangeProduct(boolean create, String id, String name, String quant){
		if((name != null) && (name.length() > 0) && (id != null) && (id.length() > 0) && (quant != null) && (quant.length() > 0)){
	    	Product prod = new Product();
	    	ProductDAO productDAO = new ProductDAO();
	    	prod.setID(Integer.parseInt(id));
	    	prod.setName(name);
	    	prod.setQuantity(Integer.parseInt(quant));
	    	if(create){
	    		if(productDAO.createCheck(Integer.parseInt(id))){
	    			return false;
	    		}
	    		productDAO.create(prod);
	    	}
	    	else{
	    		productDAO.update(prod);
	    	}
	    	return true;
    	}
    	else{
    		return false;
    	}
        
	}
	
	public static boolean deleteProd(String idToRemove){
		if((idToRemove != null) && (idToRemove.length() > 0)){
			int id = Integer.parseInt(idToRemove);
			ProductDAO productDAO = new ProductDAO();
			productDAO.delete(id);
			return true;
		}
		else{
			return false;
		}
	}
	
	public static boolean changeProductQuant(boolean add, String id, String name, int quant){
		ProductDAO productDAO = new ProductDAO();
		if((id != null) && (id.length() > 0)){
			Product product = productDAO.read(name);
			if(add){
				product.setQuantity(product.getQuantity() + quant);
			}
			else{
				product.setQuantity(product.getQuantity() - quant);
			}	
	    	productDAO.update(product);
	    	return true;
		}
		else{
			return false;
		}
	}
	
	public static void updateProductQuant(String txtProd, int quan){
		ProductDAO productDAO = new ProductDAO();
		Product product = productDAO.read(txtProd);
		product.setQuantity(product.getQuantity() + quan);
    	productDAO.update(product);
	}
	
}
