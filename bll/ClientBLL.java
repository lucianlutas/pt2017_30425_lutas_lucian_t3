package ro.tuc.pt.asig3.bll;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.table.TableModel;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import net.proteanit.sql.DbUtils;
import ro.tuc.pt.asig3.connectivity.ConnectionFactory;
import ro.tuc.pt.asig3.dao.ClientDAO;
import ro.tuc.pt.asig3.model.Client;


public class ClientBLL {
	
	public static TableModel populate(){
    	Connection conn = null;
    	ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = ConnectionFactory.getConnection();
            preparedStatement = (PreparedStatement) conn.prepareStatement("select * from client");
            rs = preparedStatement.executeQuery();
            return DbUtils.resultSetToTableModel(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
        return DbUtils.resultSetToTableModel(rs);
    }
	
	public static boolean adminChangeAcc(boolean create, String id, String name, String surname, String passw, String priv, String email){
    	if((name != null) && (name.length() > 0) && (surname != null) && (surname.length() > 0) && (passw != null) && (passw.length() > 0)&& (priv != null) && (priv.length() > 0)&& (email != null) && (email.length() > 0)){
	    	Client client = new Client();
	    	ClientDAO clientDAO = new ClientDAO();
	    	client.setID(Integer.parseInt(id));
	    	client.setName(name);
	    	client.setSurname(surname);
	    	client.setPass(passw);
	    	client.setPriv(Integer.parseInt(priv));
	    	client.setEmail(email);
	    	if(create){
	    		if(clientDAO.createCheck(Integer.parseInt(id))){
	    			return false;
	    		}
	    		clientDAO.create(client);
	    	}
	    	else{
	    		clientDAO.update(client);
	    	}
	    	return true;
    	}
    	else{
    		return false;
    	}
	}
	
	public static boolean changeAcc(ClientDAO clientDAO, int id){
		Client cleintFromDB1 = clientDAO.read(id, false);
		String name = JOptionPane.showInputDialog(null, "Name: ", cleintFromDB1.getName());
    	String surname = JOptionPane.showInputDialog(null, "Surname: ", cleintFromDB1.getSurname());
    	String passw = JOptionPane.showInputDialog(null, "Password: ", cleintFromDB1.getPass());
    	String email = JOptionPane.showInputDialog(null, "Email: ", cleintFromDB1.getEmail());
    	if((name != null) && (name.length() > 0) && (surname != null) && (surname.length() > 0) && (passw != null) && (passw.length() > 0)&& (email != null) && (email.length() > 0)){
	    	Client client = new Client();
	    	client.setID(id);
	    	client.setName(name);
	    	client.setSurname(surname);
	    	client.setPass(passw);
	    	client.setEmail(email);
	    	clientDAO.update(client);
	    	return true;
    	}
    	else{
    		return false;
    	}
	}
	
	public static boolean deleteClient(String idToRemove){
		if((idToRemove != null) && (idToRemove.length() > 0)){
			int id = Integer.parseInt(idToRemove);
			ClientDAO clientDAO = new ClientDAO();
			clientDAO.delete(id);
			return true;
		}
		else{
			return false;
		}
	}
	
	public static Client returnClient(int id){
		Client client = new Client();
		ClientDAO clientDAO = new ClientDAO();
		client = clientDAO.read(id, false);
		return client;
	}
}
