package ro.tuc.pt.asig3.model;

public class Order {
	private int id;
	private int cartID;
	
	public int getID(){
		return id;
	}
	
	public void setID(int id){
		this.id = id;
	}
	
	public int getCartID(){
		return cartID;
	}
	
	public void setCartID(int cartID){
		this.cartID = cartID;
	}
}
