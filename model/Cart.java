package ro.tuc.pt.asig3.model;

public class Cart {
	private int id;
	private int itemNo;
	private String prodName;
	private int prodQuantity;
	
	public int getID(){
		return id;
	}
	
	public void setID(int id){
		this.id = id;
	}
	
	public int getItemNo(){
		return itemNo;
	}
	
	public void setItemNo(int itemNo){
		this.itemNo = itemNo;
	}
	
	public String getProdName(){
		return prodName;
	}
	
	public void setProdName(String prodName){
		this.prodName = prodName;
	}
	
	public int getProdQuantity(){
		return prodQuantity;
	}
	
	public void setProdQuantity(int prodQuantity){
		this.prodQuantity = prodQuantity;
	}
}
