package ro.tuc.pt.asig3.model;

public class Client {
	
	private int id;
	private String name;
	private String surname;
	private String pass;
	private int priv;
	private String email;
	
	public int getID(){
		return id;
	}
	
	public void setID(int id){
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getSurname(){
		return surname;
	}
	
	public void setSurname(String surname){
		this.surname = surname;
	}
	
	public String getPass(){
		return pass;
	}
	
	public void setPass(String pass){
		this.pass = pass;
	}
	
	public int getPriv(){
		return priv;
	}
	
	public void setPriv(int priv){
		this.priv = priv;
	}
	
	public String getEmail(){
		return email;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
}
