package ro.tuc.pt.asig3.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.table.TableModel;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import ro.tuc.pt.asig3.connectivity.ConnectionFactory;
import ro.tuc.pt.asig3.model.Product;

import net.proteanit.sql.DbUtils;

public class ProductDAO {
	
	private static final String CREATE_QUERY = "INSERT INTO product (`prouctID`, `productName`, `productQuantity`) VALUES (?,?,?)";
    /** The query for read. */
    private static final String READ_QUERY = "SELECT * FROM product where prouctID = ?";
    /** The query for update. */
    private static final String UPDATE_QUERY = "UPDATE product SET productName=?, productQuantity=? WHERE prouctID=?";
    /** The query for delete. */
    private static final String DELETE_QUERY = "DELETE FROM product WHERE prouctID = ?";
    
    public int create(Product prod) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = ConnectionFactory.getConnection();
            preparedStatement = (PreparedStatement) conn.prepareStatement(CREATE_QUERY,
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, prod.getID());
            preparedStatement.setString(2, prod.getName());
            preparedStatement.setInt(3, prod.getQuantity());
            preparedStatement.execute();
            result = preparedStatement.getGeneratedKeys();
 
            if (result.next() && result != null) {
                return result.getInt(1);
            } else {
                return -1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
               sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
 
        return -1;
    }
    
    public Product read(int id) {
        Product prod = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = ConnectionFactory.getConnection();
            preparedStatement = (PreparedStatement) conn.prepareStatement(READ_QUERY);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
            if (result.next() && result != null) {
            	prod = new Product();
            	prod.setID(result.getInt(1));
            	prod.setName(result.getString(2));
            	prod.setQuantity(result.getInt(3));
            } else {
                // TODO
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
               rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
 
        return prod;
    }
    
    public Product read(String name) {
        Product prod = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = ConnectionFactory.getConnection();
            preparedStatement = (PreparedStatement) conn.prepareStatement("SELECT * FROM product where productName = ?");
            preparedStatement.setString(1, name);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
            if (result.next() && result != null) {
            	prod = new Product();
            	prod.setID(result.getInt(1));
            	prod.setName(result.getString(2));
            	prod.setQuantity(result.getInt(3));
            } else {
                // TODO
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
               rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
 
        return prod;
    }
    
    public boolean update(Product prod) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = ConnectionFactory.getConnection();
            preparedStatement = (PreparedStatement) conn.prepareStatement(UPDATE_QUERY);
            preparedStatement.setInt(3, prod.getID());
            preparedStatement.setString(1, prod.getName());
            preparedStatement.setInt(2, prod.getQuantity());
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
        return false;
    }
    
    public boolean delete(int id) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = ConnectionFactory.getConnection();
            preparedStatement = (PreparedStatement) conn.prepareStatement(DELETE_QUERY);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
        return false;
    }
    
    public static TableModel populate(){
    	Connection conn = null;
    	ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = ConnectionFactory.getConnection();
            preparedStatement = (PreparedStatement) conn.prepareStatement("select * from product");
            rs = preparedStatement.executeQuery();
            return DbUtils.resultSetToTableModel(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
        return DbUtils.resultSetToTableModel(rs);
    }
    
    public boolean createCheck(int id) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = ConnectionFactory.getConnection();
            preparedStatement = (PreparedStatement) conn.prepareStatement("SELECT * FROM product where prouctID = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
            if (result.next() && result != null) {
            	return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
               rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
 
        return false;
    }
	
}
